#!/usr/bin/env python

class Foo(object):
    def __init__(self):
        self.msg = u'bar'

    def __unicode__(self):
        return self.msg

    def __eq__(self, _unicode):
        return self.msg == _unicode


if __name__ == '__main__':
    assert Foo() == 'bar'
