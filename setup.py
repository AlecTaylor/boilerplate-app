from setuptools import setup, find_packages

find_and_parse_packages = lambda package_name: filter(lambda x: x != 'find' and not x.endswith('.src'),
                                                      [package_name + '.' + p.replace('src.', '')
                                                       for p in find_packages()] + [package_name])

setup(name='simple_app', version='0.3',
      author='Alec Taylor', py_modules=['simple_app'],
      test_suite='tests')
