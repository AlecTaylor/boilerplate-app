from simple import Foo
from unittest import TestCase, main as unittest_main


class TestSimpleFoo(TestCase):
    def setUp(self):
        pass

    def test_simple_foo(self):
        self.assertEqual(Foo(), 'bar')


if __name__ == '__main__':
    unittest_main()
